package com.root55.meals_app.WepService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WepServiceClient {
    private static final  String base_url = "https://www.themealdb.com/api/json/v1/1/" ;
    private static Retrofit retrofit = null ;
    public static  Retrofit getRetrofit(){
        if(retrofit == null ){
            retrofit = new Retrofit.
                    Builder().baseUrl(base_url).
                    addConverterFactory(GsonConverterFactory.
                            create()).build() ;
        }
        return  retrofit ;
    }
}
