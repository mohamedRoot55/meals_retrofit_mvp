package com.root55.meals_app.WepService;

import com.root55.meals_app.Home.pojo.CategoryResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("categories.php")
    Call<CategoryResponse> getCategories() ;
}
