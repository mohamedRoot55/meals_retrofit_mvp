package com.root55.meals_app.Home;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.root55.meals_app.Home.pojo.Category;
import com.root55.meals_app.Home.pojo.CategoryResponse;
import com.root55.meals_app.WepService.ApiService;
import com.root55.meals_app.WepService.WepServiceClient;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryViewModel extends ViewModel {
    MutableLiveData<ArrayList<Category>> mCategories = new MutableLiveData<>() ;


    public void getDataFromDB(){
        CallCategoryApi();
    }

    private void CallCategoryApi() {

        ApiService apiService = WepServiceClient.getRetrofit().create(ApiService.class) ;
        Call<CategoryResponse> call = apiService.getCategories() ;
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {

                assert response.body() != null;
                mCategories.setValue((ArrayList<Category>) response.body().getCategories());
                Log.e("response" , response.body().getCategories().get(1).getStrCategoryThumb().toString());

            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Log.e("no response" , Objects.requireNonNull(t.getMessage()));
            }
        });
    }
}
