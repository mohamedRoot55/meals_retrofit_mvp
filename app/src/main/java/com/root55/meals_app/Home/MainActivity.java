package com.root55.meals_app.Home;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;


import com.root55.meals_app.Home.pojo.Category;
import com.root55.meals_app.R;
import com.root55.meals_app.adapters.CategoryAdapterHorizontal;
import com.root55.meals_app.adapters.CategoryAdapterVertical;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    RecyclerView mCategory_rv_Horizontal ,mCategory_rv_Vertical ;

    RecyclerView.LayoutManager layoutManagerHorizontal , layoutManagerVertical  ;
    CategoryAdapterHorizontal adapter_Horizontal;
    CategoryAdapterVertical adapter_Vertical ;
    CategoryViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        getCategoryData();
    }

    private void initViews() {

        mCategory_rv_Horizontal = findViewById(R.id.categories_recycler_horizontal);
        mCategory_rv_Vertical = findViewById(R.id.categories_recycler_vertical) ;
        layoutManagerHorizontal = new LinearLayoutManager(MainActivity.this, RecyclerView.HORIZONTAL, false);
        layoutManagerVertical  = new GridLayoutManager(MainActivity.this , 3);
    }


    private void getCategoryData() {
        viewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);


        viewModel.getDataFromDB();


        viewModel.mCategories.observe(this, new Observer<ArrayList<Category>>() {
            @Override
            public void onChanged(ArrayList<Category> categories) {
                adapter_Horizontal = new CategoryAdapterHorizontal(categories, MainActivity.this);
                adapter_Vertical = new CategoryAdapterVertical(categories, MainActivity.this);


                mCategory_rv_Horizontal.setAdapter(adapter_Horizontal);
                mCategory_rv_Horizontal.setLayoutManager(layoutManagerHorizontal);
                mCategory_rv_Vertical.setAdapter(adapter_Vertical);
                mCategory_rv_Vertical.setLayoutManager(layoutManagerVertical);
            }
        });
    }


}
