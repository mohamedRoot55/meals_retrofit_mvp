package com.root55.meals_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.root55.meals_app.Home.pojo.Category;
import com.root55.meals_app.R;

import java.util.ArrayList;

public class CategoryAdapterVertical extends RecyclerView.Adapter<CategoryAdapterVertical.CategoryViewHolder> {
    private ArrayList<Category> response;
    private Context context;

    public CategoryAdapterVertical(ArrayList<Category> response, Context context) {
        this.response = response;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryAdapterVertical.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_vertical, parent, false);
        CategoryAdapterVertical.CategoryViewHolder viewHolder = new CategoryAdapterVertical.CategoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapterVertical.CategoryViewHolder holder, int position) {
        holder.textView.setText(response.get(position).getStrCategory());
        Glide.with(context).load(response.get(position).getStrCategoryThumb()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView ;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cat_img);
            textView = itemView.findViewById(R.id.category_name) ;
        }
    }
}

