package com.root55.meals_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.root55.meals_app.Home.pojo.Category;

import com.root55.meals_app.R;

import java.util.ArrayList;

public class CategoryAdapterHorizontal extends RecyclerView.Adapter<CategoryAdapterHorizontal.CategoryViewHolder> {
    private ArrayList<Category> response;
    private Context context;

    public CategoryAdapterHorizontal(ArrayList<Category> response, Context context) {
        this.response = response;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_horizontal, parent, false);
        CategoryViewHolder viewHolder = new CategoryViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Glide.with(context).load(response.get(position).getStrCategoryThumb()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.cat_img_horizontal);
        }
    }
}
